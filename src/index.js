import React, { useState, useEffect, useCallback } from "react";
import ReactDOM from "react-dom";
import axios from "axios";

import { Header } from "./components/Header";
import { TodoList } from "./components/TodoList";
import { TodoForm } from "./components/TodoForm";

import "./styles.css";

const App = () => {
  const [todos, setTodos] = useState([]);
  const [editingId, setEditingId] = useState("");

  const loadTodos = useCallback(async () => {
    const response = await axios.get("http://localhost:3001/todos");
    setTodos(response.data);
    console.log(response.data);
  }, []);

  useEffect(() => {
    loadTodos();
  }, [loadTodos]);

  const addTodo = async (todo) => {
    // setTodos([...todos, todo]);
    const newTodo = {
      name: todo.name,
      description: todo.description,
      dueDate: todo.dueDate,
      completed: false,
      favorite: false
    };

    await axios.post("http://localhost:3001/todos/", newTodo);
    loadTodos();
  };

  const removeTodo = async (id) => {
    // const updateTodos = todos.filter(prvokPola => prvokPola.name !== name);
    // setTodos(updateTodos);
    await axios.delete("http://localhost:3001/todos/" + id);
    loadTodos();
  };

  const completeTodo = async (id) => {
    // setTodos(todos.map((x) => {
    //   if (x.name === name) {
    //     return {...x, completed: true};
    //   } else return x;
    // }))
    await axios.put("http://localhost:3001/todos/" + id, { completed: true });
    loadTodos();
  };

  const favoriteTodo = async (todo) => {
    const newValue = !todo.favorite;
    await axios.put(`http://localhost:3001/todos/${todo.id}`, {favorite: newValue} );
    loadTodos();
  };

  const updateTodo = async (todo) => {
    await axios.put(`http://localhost:3001/todos/${todo.id}`, todo);
    setEditingId("");
    loadTodos();
  };

  const undoTodo = async (id) => {
    await axios.put("http://localhost:3001/todos/" + id, { completed: false });
    loadTodos();
  };

  const removeAll = async () => {
    // setTodos([]);
    await axios.delete("http://localhost:3001/todos/delete/all");
    loadTodos();
  };

  const startEditing = (id) => {
    setEditingId(id);
  };

  const getMeTodoThatIEdit = () => {
    const todo = todos.find((todo) => todo.id === editingId);
    return todo;
  };

  return (
    <div className="app">
      <div className="container">
        <Header title="TODO LIST" isVisible={true} ></Header>
        <div className="content">
          <div className="todolistCont">
            <TodoList todos={todos} onRemove={removeTodo} onComplete={completeTodo} onUndo={undoTodo} onEdit={startEditing} onRemoveAll={removeAll} onFavorite={favoriteTodo} />
          </div>
          <TodoForm todo={getMeTodoThatIEdit()} onAdd={addTodo} onUpdate={updateTodo}></TodoForm>
        </div>
      </div>
    </div>
  );
};

ReactDOM.render(<App />, document.getElementById("root"));
