import React from "react";
import "./Todo.css";

import checkmark from "../images/checkmark.png";
import bin from "../images/bin.jpg";
import undo from "../images/undo.png";

import dayjs from "dayjs";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import FavoriteIcon from "@material-ui/icons/Favorite";
import MoreVertIcon from "@material-ui/icons/MoreVert";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import Button from "@material-ui/core/Button";
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import CheckBoxIcon from '@material-ui/icons/CheckBox';
import Favorite from '@material-ui/icons/Favorite';
import FavoriteBorder from '@material-ui/icons/FavoriteBorder';

export const Todo = (props) => {
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleEdit = () => {
    handleClose();
    props.onEdit(props.todo.id);
  };

  const handleRemove = () => {
    handleClose();
    props.onRemove(props.todo.id);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <Card>
      <CardHeader
        action={
          <IconButton
            aria-label="settings"
            onClick={(event) => {
              setAnchorEl(event.currentTarget);
            }}>
            <MoreVertIcon />
          </IconButton>
        }
        title={props.todo.name}
        subheader={`Due on ${dayjs(props.todo.dueDate).format("DD.MM.YYYY")}`}
      />
      <Menu id="simple-menu" anchorEl={anchorEl} keepMounted open={Boolean(anchorEl)} onClose={handleClose}>
        <MenuItem onClick={handleEdit}>Edit</MenuItem>
        <MenuItem onClick={handleRemove}>Delete</MenuItem>
      </Menu>
      <CardContent>
        <Typography color="textSecondary" component="p">
          {props.todo.description}
        </Typography>
        {props.todo.completed && <p>✅</p>}
      </CardContent>
      <CardActions disableSpacing>
        {props.todo.completed ? (
          <div
            className="undoButton"
            onClick={() => {
              return props.onUndo(props.todo.id);
            }}>
            <img src={undo} />
          </div>
        ) : (
          <div
            className="checkmarkButton"
            onClick={() => {
              return props.onComplete(props.todo.id);
            }}>
            <img src={checkmark} />
          </div>
        )}
        <FormControlLabel
        control={<Checkbox checked={props.todo.favorite} onClick={() => {
          return props.onFavorite(props.todo);
        }} icon={<FavoriteBorder />} checkedIcon={<Favorite />} name="checkedH" />}
      />
      </CardActions>
    </Card>
  );
};
