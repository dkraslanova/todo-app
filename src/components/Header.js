import React from "react";
import "./Header.css";
import { Quotes } from "./Quotes";

export const Header = (props) => {
  if (props.isVisible === true) {
    return (
      <div className="header">
        <h1 className="title">{props.title}</h1>
        <div >
          <Quotes />
        </div>
      </div>
    );
  } else {
    return null;
  }
};
