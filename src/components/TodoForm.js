import React, { useEffect, useState } from "react";
import "./TodoForm.css";

export const TodoForm = (props) => {
  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [dueDate, setDueDate] = useState(
    new Date().toISOString().substring(0, 10)
  );

  useEffect(() => {
    if (props.todo) {
      setName(props.todo.name);
      setDescription(props.todo.description);
      setDueDate(props.todo.dueDate);
    }
  }, [props.todo]);

  const onFormSubmit = (e) => {
    e.preventDefault(); // prevent full page refresh
    if (props.todo === undefined) {
      props.onAdd({name, description, dueDate});
    } else {
      props.onUpdate({id: props.todo.id, name, description, dueDate});
    }
    setName("");
    setDescription("");
    setDueDate("");
  };

  return (
    <div className="todo-form">
      <h4>{props.todo ? "Edit" : "Add"} TODO</h4>
      <form onSubmit={onFormSubmit}>
        <input
          className="form-input"
          type="text"
          value={name}
          onChange={(e) => setName(e.target.value)}
          placeholder="Task name"
        />
        <textarea
          className="form-text"
          value={description}
          onChange={(e) => setDescription(e.target.value)}
          placeholder="Description"
        ></textarea>
        <input
          className="form-date"
          type="date"
          value={dueDate}
          onChange={(e) => setDueDate(e.target.value)}
          placeholder="Add due date of your task"
        ></input>
        <button
          className="button button-add"
          disabled={
            name != "" && description != "" && dueDate != "" ? false : true
          }
        >
          {props.todo ? "Edit" : "Add"} TODO
        </button>
      </form>
    </div>
  );
};
