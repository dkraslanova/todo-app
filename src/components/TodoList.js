import React from "react";
import "./TodoList.css";
import { Todo } from "./Todo";

import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";

let ID = 1;
const getId = () => ID++;

export const TodoList = (props) => {
  const todos = props.todos;

  const [openDialog, setOpenDialog] = React.useState(false);
  const handleClickOpenDialog = () => {
    setOpenDialog(true);
  };
  const handleCloseDialog = () => {
    setOpenDialog(false);
  };
  const handleDelete = () => {
    props.onRemoveAll();
    handleCloseDialog();
  };
  if (todos.length <= 0) {
    return (
      <div className="todo-nothing">
        <p>Nothing to do</p>
      </div>
    );
  } else {
    return (
      <div className="todo-list">
        <div className="todo-header">
          <div >
            <h4 className="todo-header-text">Your TODO list</h4>
            <p>Total: {todos.length}</p>
          </div>
          <button className="remove-all" onClick={handleClickOpenDialog}>
            Remove All
          </button>
        </div>
        {todos.map((todo) => {
          return (
            <div key={todo.id} className="todo-row">
              <Todo
                todo={todo}
                onRemove={props.onRemove}
                onUndo={props.onUndo}
                onComplete={props.onComplete}
                onEdit={props.onEdit}
                onFavorite={props.onFavorite}
              />
            </div>
          );
        })}
        <Dialog
          open={openDialog}
          onClose={handleCloseDialog}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description">
          <DialogTitle id="alert-dialog-title">Are you sure you want to delete all TODOs?</DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
              Deleting all todo items cannot be reverted.
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={handleDelete} color="primary" autoFocus>
              YES
            </Button>
            <Button onClick={handleCloseDialog} color="primary">
              NO
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
};
